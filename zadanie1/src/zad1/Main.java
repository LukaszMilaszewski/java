package zad1;

import java.util.ArrayList;

public class Main {
	
	public static void main(String [ ] args) {
		
		GUI gui = new GUI();
		gui.perform();
	}
	
	static String calculate(ArrayList<Integer> arr) {
		
		arr.add(0);
		arr.add(1);
		int iter = 2;
		int max = 0;
		int result = 0;
		
		while (max < 4000000) {
			arr.add(arr.get(iter-1) + arr.get(iter-2));
			max = arr.get(iter);
			iter++;
			if (max % 2 == 0)
				result += max;
		}	
		String sum;
		sum = String.valueOf(result);
		
		return sum;
	}
	
	
}