package lab4;

public class Game {
	
	static void performPaddleMove(Player player, int height) {
		 if (player.getUpPressed()) 
	 			if (player.getY() - player.getSpeed() > 0)
	 				player.setY(player.getY() - player.getSpeed());
	 		
		 if(player.getDownPressed()) 
	 			if (player.getY() + player.getSpeed() + player.getH() < height)
	 				player.setY(player.getY() + player.getSpeed());
	}
	
	static void performBallMove(Player player1, Player player2, Ball ball, int height) {	
		int nextballLeft = ball.getX() + ball.getXspeed();
        int nextballRight = ball.getX() + ball.getD() + ball.getXspeed();
        int nextballTop = ball.getY() + ball.getYspeed();
        int nextballBottom = ball.getY() + ball.getD() + ball.getYspeed();
        
        int playerOneRight = player1.getX() + player1.getW();
        int playerOneTop = player1.getY();
        int playerOneBottom = player1.getY() + player1.getH();
        
        int playerTwoLeft = player2.getX();
        int playerTwoTop = player2.getY();
        int playerTwoBottom = player2.getY() + player2.getH();
        if(GUI.playView) {
	        // odbicie od gory albo dolu ekranu
	        if (nextballTop < 0 || nextballBottom > height) {
	        	ball.revertYspeed();
	        	Sound.sound("pong.wav");
	        }
	        
	        // lewa strona
	        if (nextballLeft < playerOneRight) {
	        	if (nextballTop > playerOneBottom || nextballBottom < playerOneTop) 
	        		performRoundFinish(player2, ball);
	        	 else {
	        		ball.revertXspeed();
	        		Sound.sound("pong.wav");
	        	 }
	        }
	        
	        // prawa strona
	        if (nextballRight > playerTwoLeft) {
	        	if (nextballTop > playerTwoBottom || nextballBottom < playerTwoTop) 
	        		performRoundFinish(player1, ball);
	        	 else { 
	        		ball.revertXspeed();
	        		Sound.sound("pong.wav");
	        		
	        	 }
	        }  
	        ball.changePosition();  
        }
	}
        
    static void performRoundFinish(Player player, Ball ball) {
    	player.setScore(player.getScore() + 1);
		if (player.getScore() == 2) {
			GUI.playView = false;
			GUI.stopView = true;	
		}
		ball.defaultPosition();
		ball.revertSpeed();
    }   

    static void startingValues(Player player1, Player player2, Ball ball) {
    	GUI.stopView = false;
		GUI.startView = true;
		player1.setY(110);
		player1.setScore(0);
		player2.setY(110);
		player2.setScore(0);
		ball.setX(240);
		ball.setY(125);	
    }
}