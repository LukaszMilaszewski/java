package lab4;

public class BallThread implements Runnable {

	private Object obj;
	private Player player1;
	private Player player2;
	private Ball ball;
	private int height;
	
	BallThread(Object obj, Player player1, Player player2, Ball ball, int height) {
		this.obj = obj;
		this.player1 = player1;
		this.player2 = player2;
		this.ball = ball;
		this.height = height;
	}
	
	@Override
	public void run() {
		synchronized (obj) {
			try {
				Game.performBallMove(player1, player2, ball, height);
				obj.wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
