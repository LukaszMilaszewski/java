package lab4;

public class Main {

    public static void main(String[] args) {
    	Object object = new Object();
    	Player player1 = new Player(25, 110, 10, 50, 5);
    	Player player2 = new Player(455, 110, 10, 50, 5);
    	Ball ball = new Ball(240, 125, -3, 3, 20);	
    	GUIWrapper guiWrapper = new GUIWrapper(player1, player2, ball);
    }
}