package lab4;

public class PlayerThread implements Runnable {
	
	Object obj;
	Player player;
	int height;
	
	public PlayerThread(Object obj, Player player, int height) {
		this.obj = obj;
		this.player = player;
		this.height = height;
	}
	
	@Override
	public void run() {
		synchronized (obj) {
            try {
				Game.performPaddleMove(player, height);
				obj.wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
        }	
	}
}
