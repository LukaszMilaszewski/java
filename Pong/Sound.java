package lab4;

import javax.sound.sampled.*;
import java.net.*;
public class Sound {

	public static void sound(String fn) {
		try {
			URL url = Sound.class.getResource(fn);
			AudioInputStream ais = AudioSystem.getAudioInputStream(url);
			Clip clip = AudioSystem.getClip();
			clip.open(ais);
			clip.start(); 
		} 
		catch(Exception e){
			e.printStackTrace(); 
		}
	}
}
