package lab4;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JPanel;
import javax.swing.Timer;

public class GUI extends JPanel implements ActionListener, KeyListener {
	
	static boolean playView = false;
	static boolean stopView = false;
	static boolean startView = true;
	private Player player1;
	private Player player2;
	private Ball ball;

	PlayerThread leftPlayerThread;
	PlayerThread rightPlayerThread;
	BallThread ballThread;
	Object obj;
	
    public GUI(Player player1, Player player2, Ball ball) {
    	this.player1 = player1;
    	this.player2 = player2;
    	this.ball = ball;
    	
    	obj = new Object();
    	leftPlayerThread = new PlayerThread(obj, player1, getHeight());
    	rightPlayerThread = new PlayerThread(obj, player2, getHeight());
    	ballThread = new BallThread(obj, player1, player2, ball, getHeight());
    	
    	new Thread(leftPlayerThread).start();
    	new Thread(rightPlayerThread).start();
    	new Thread(ballThread).start();
    	
    	setBackground(Color.GRAY);
    	
    	setFocusable(true);
    	addKeyListener(this);
    	
    	Timer timer = new Timer(1000/60, this);
    	timer.start();
    }
    
    public void paintComponent(Graphics g) {
    	super.paintComponent(g);
    	g.setColor(Color.WHITE);
   	
    	if (startView) {
    		drawStartView(g);
        } else if (playView) {
        	drawPlayView(g, player1, player2, ball);	
        } else if (stopView) {
        	ball.defaultPosition();
        	drawStopView(g, player1, player2);        	
        }	
    }

	@Override
	public void actionPerformed(ActionEvent e) {
		int height = getHeight();
		
		synchronized(obj) {
			obj.notifyAll();
		}
		/*
		Game.performPaddleMove(player1, height);
		Game.performPaddleMove(player2, height);
		Game.performBallMove(player1, player2, ball, height); 
		*/
        repaint();		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		checkMoveKeys(e, player1, player2, true);
	}

	@Override
	public void keyReleased(KeyEvent e) {	
		if (startView) 
			checkStartKey(e);
		else if (playView) 
			checkMoveKeys(e, player1, player2, false);
		else if (stopView) 
			restart(e, player1, player2, ball);		
	}

	@Override
	public void keyTyped(KeyEvent e) {}

	void checkStartKey(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_P) {
			startView = false;
			playView = true;
		}
	}

	void checkMoveKeys(KeyEvent e, Player player1, Player player2, boolean key) {	
		switch(e.getKeyCode()) {
			case KeyEvent.VK_UP:
				player2.setUpPressed(key);
				break;
			case KeyEvent.VK_DOWN:
				player2.setDownPressed(key);
				break;
			case KeyEvent.VK_W:
				player1.setUpPressed(key);
				break;
			case KeyEvent.VK_S:
				player1.setDownPressed(key);
				break;
		}
	}
	
	void restart(KeyEvent e, Player player1, Player player2, Ball ball) {
		if (e.getKeyCode() == KeyEvent.VK_SPACE) 
			Game.startingValues(player1, player2, ball);
	}
	
	void drawStartView(Graphics g) {
		g.setFont(new Font(Font.DIALOG, Font.BOLD, 36));
        g.setFont(new Font(Font.DIALOG, Font.BOLD, 36));
        g.drawString("Pong", 200, 100);
        g.setFont(new Font(Font.DIALOG, Font.BOLD, 18));
        g.drawString("Press 'P' to Play.", 175, 200);	
	}
	
	void drawPlayView(Graphics g, Player player1, Player player2, Ball ball) {
		int playerOneRight = player1.getX() + player1.getW();
        int playerTwoLeft =  player2.getX();
        
        for (int lineY = 0; lineY < getHeight(); lineY += 50) {
            g.drawLine(250, lineY, 250, lineY+25);
        }
      //  g.drawLine(playerOneRight, 0, playerOneRight, getHeight());
      //  g.drawLine(playerTwoLeft, 0, playerTwoLeft, getHeight());
        
        g.setFont(new Font(Font.DIALOG, Font.BOLD, 36));
        g.drawString(String.valueOf(player1.getScore()), 100, 100);
        g.drawString(String.valueOf(player2.getScore()), 364, 100);
        
    	g.fillOval(ball.getX(), ball.getY(), ball.getD(), ball.getD());
    	g.fillRect(player1.getX(), player1.getY(), player1.getW(), player1.getH());
    	g.fillRect(player2.getX(), player2.getY(), player2.getW(), player2.getH());
	}
	
	void drawStopView(Graphics g, Player player1, Player player2) {
    	g.setFont(new Font(Font.DIALOG, Font.BOLD, 36));
        g.drawString(String.valueOf(player1.getScore()), 75, 100);
        g.drawString(String.valueOf(player2.getScore()), 400, 100);

        g.setFont(new Font(Font.DIALOG, Font.BOLD, 36));
        if (player1.getScore() > player2.getScore()) 
            g.drawString("Player 1 Wins!", 125, 150);
        else 
            g.drawString("Player 2 Wins!", 125, 150);

        g.setFont(new Font(Font.DIALOG, Font.BOLD, 18));
        g.drawString("Press space to restart.", 145, 200);
	}
}
