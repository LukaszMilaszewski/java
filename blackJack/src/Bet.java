
public class Bet {
    private double bet;
    private double insuranceValue;
    
    Bet(double bet) {
        this.bet = bet;
        setInsuranceValue(0);
    }
    public double getBet() {
        return bet;
    }

    public void setBet(double d) {
        this.bet = d;
    }
    public double getInsuranceValue() {
        return insuranceValue;
    }
    public void setInsuranceValue(double d) {
        this.insuranceValue = d;
    }
    
}
