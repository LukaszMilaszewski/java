import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class GUI {
        
    public void perform() {
                
        JFrame window = setFrame();
        JPanel panel = setPanel(window);
        
        JTextField betText = new JTextField("Value");
        
        JPanel subPanelSouth = setSubPanel();
        JPanel subPanelNorth = setSubPanel();
        JPanel subPanelEast = setSubPanel();
        JPanel subPanelWest = setSubPanel();
        
        JLabel labelNorth = new JLabel();
        JLabel labelEast = new JLabel();
        JLabel labelWest = new JLabel();
        JLabel betLabel = new JLabel();
        JLabel humanCards = new JLabel();
        JLabel croupierCards = new JLabel();
        
        // NORTH        
        addToSubPanel(subPanelNorth, setLabel("Your cards:                                ", labelEast));
        addToSubPanel(subPanelNorth, setLabel("Your bet: ", labelNorth));
        addToSubPanel(subPanelNorth, setLabel("     ",betLabel));
        addToSubPanel(subPanelNorth, setLabel("                           Croupier cards: ", labelWest));
        
        // EAST
        addToSubPanel(subPanelEast, setLabel("111111111", humanCards));
        
        // WEST
        addToSubPanel(subPanelWest, setLabel("0000000", croupierCards));
        
        // SOUTH
        JButton hitButton = setHitButton();
        JButton stopButton = setStopButton();
        
        addBetText(subPanelSouth, betText);
        addToSubPanel(subPanelSouth, setBetButton(subPanelSouth, betText, betLabel, hitButton, stopButton));
        addToSubPanel(subPanelSouth, hitButton);
        addToSubPanel(subPanelSouth, stopButton);
        addToSubPanel(subPanelSouth, setInsuranceButton(subPanelSouth, betLabel));
        
        // set main panel
        setNorthPanel(subPanelNorth, panel);
        setEastPanel(subPanelEast, panel);
        setWestPanel(subPanelWest, panel);
        setSouthPanel(subPanelSouth, panel);
    }
    
    JLabel setLabel(String text, JLabel label) {
        label.setText(text);
        return label;
    }
    
    void addInsuranceText(JPanel subPanel, JTextField textField) {
        textField.setVisible(false);
        subPanel.add(textField);
    }
    
    void addBetText(JPanel subPanel, JTextField textField) {
        textField.setVisible(true);
        subPanel.add(textField);
    } 
    
    void setNorthPanel(JPanel subPanel, JPanel panel) {
        panel.add(subPanel, BorderLayout.NORTH);
    }
    
    void setSouthPanel(JPanel subPanel, JPanel panel) {
        panel.add(subPanel, BorderLayout.SOUTH);
    }
    
    void setEastPanel(JPanel subPanel, JPanel panel) {
        panel.add(subPanel, BorderLayout.EAST);
        
    }
    
    void setWestPanel(JPanel subPanel, JPanel panel) {
        panel.add(subPanel, BorderLayout.WEST);
    }
    
    void addToSubPanel(JPanel subPanel, JButton button) {
        subPanel.add(button);
    }
    
    void addToSubPanel(JPanel subPanel, JLabel label) {
        subPanel.add(label);
    }
    
    void addToSubPanel(JPanel subPanel, JTextField textField) {
        subPanel.add(textField);
    }
    
    JPanel setSubPanel() {
        return new JPanel();
    }
    
    JFrame setFrame() {
        JFrame window = new JFrame("Black Jack");
        window.setLocationRelativeTo(null);
        window.setVisible(true);
        window.setSize(450,150);
        window.setResizable(false);
        
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        return window;
    }
    
    JPanel setPanel(JFrame window) {
        JPanel panel = new JPanel(new BorderLayout());
        window.add(panel);
        return panel;
    }
    
    /*
    JLabel setLabel(JPanel c) {
        JLabel label = new JLabel("", SwingConstants.CENTER);
        label.setBounds(0, 10, 300, 20);
        c.add(label, BorderLayout.NORTH);
        label.setText("bbbB");
            
        return label;
    }
    */
    JButton setHitButton() {
      JButton hitButton = new JButton("Hit");
      hitButton.setVisible(false);
      
      hitButton.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
          // TO-DO
          }
      });
      return hitButton;
    }
    
    JButton setStopButton() {
        JButton stopButton = new JButton("Stop");
        stopButton.setVisible(false);
        
        stopButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // TO-DO
            }
        });   
        return stopButton;
    }
    
    JButton setInsuranceButton(JPanel subPanel, JLabel label) {
        JButton insurance = new JButton("Insurance");
        insurance.setVisible(false);    
        insurance.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String text = label.getText();
                int value = Integer.valueOf(text) / 2 ;
                label.setText(String.valueOf(value + Integer.valueOf(text)));   
                
                subPanel.revalidate();
                subPanel.repaint();
           }
        });      
        return insurance;
    }
    
    JButton setBetButton(JPanel  subPanel, JTextField betText, JLabel label, JButton hit, JButton stop) {
        JButton bet = new JButton("Bet");
        
        bet.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                
                String text = betText.getText();
                label.setText(text);
                bet.setVisible(false);
                betText.setVisible(false);
                hit.setVisible(true);
                stop.setVisible(true);
                subPanel.revalidate();
                subPanel.repaint();
            }
        });  
        return bet;   
    }
} 