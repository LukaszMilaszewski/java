import java.util.ArrayList;

public class Player {
    ArrayList<Card> player = new ArrayList<Card>();
    
    
    Player(Pot pot) {
        Card firstCard = pot.getCard(49);
        Card secondCard = pot.getCard(40);
        
        addCard(firstCard);
        addCard(secondCard);
    }
    Player(Pot pot, int i) {
        Card firstCard = pot.getCard(49);
        Card secondCard = pot.getCard(2);
        // as to 49
        addCard(firstCard);
        addCard(secondCard);
     //   addCard(pot.getCard(40));
     //   addCard(pot.getCard(40));
     //   addCard(pot.getCard(40));    
    }
    
   /* Player(Pot pot) {
        Card firstCard = pot.getRandom();
        Card secondCard = pot.getRandom();
        
        addCard(firstCard);
        addCard(secondCard);
    }
    */
   void addCard(Card card) {
       player.add(card);
   }
   
   int getSize() {
       return player.size();
   }
   void removeCard(int i) {
       player.remove(i);     
   }
   
   int getValue(int i) {
       return player.get(i).getCardValue();
   }
   
   void printCard(int i) {
       System.out.print(getValue(i)+" ");
   }
   
} 
